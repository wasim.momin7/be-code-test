-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2023 at 04:48 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `be-code-test`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2019_12_20_094217_create_organisations_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('1febbf9efca1f23bfdc3c09f71f948f0ddbab95af98af9888613e84b985ca010290fb79deb43dc26', 13, 1, NULL, '[]', 0, '2023-06-26 07:47:00', '2023-06-26 07:47:00', '2024-06-26 13:17:00'),
('34ce7310ea003d101474ba23e430734c5e09b3dab1ff521efc11547e493360e52c1c801f8ee3ff73', 13, 1, NULL, '[]', 0, '2023-06-10 02:05:29', '2023-06-10 02:05:29', '2024-06-10 07:35:29'),
('3cbf63de05ee4c8fca851089880884f8b7192457ed70de36b856eac8bc225704412ab04c1e9ba3a4', 13, 1, NULL, '[]', 0, '2023-06-10 02:09:06', '2023-06-10 02:09:06', '2024-06-10 07:39:06'),
('48db8fafc31f1d482f389f799eacdc4c065c3bab236eec80099c062e70482c3a0eaab5ec54a3ccfd', 13, 1, NULL, '[]', 0, '2023-06-26 07:44:09', '2023-06-26 07:44:09', '2024-06-26 13:14:09'),
('5ce05e28ae6f97b22755b31948a521699ad8fd5c38d8fefd596431e9737f055e4bd12c85910dce6e', 13, 1, NULL, '[]', 0, '2023-06-10 03:25:06', '2023-06-10 03:25:06', '2024-06-10 08:55:06'),
('b076e06be9068c918cc895d65e07b4e7e836f5be59bd22a9a8046b6813715a76ee262ded728aac9f', 13, 1, NULL, '[]', 0, '2023-06-10 01:59:54', '2023-06-10 01:59:54', '2024-06-10 07:29:54'),
('d01b2be3b7ce79fc38894f17f3052f300bbf41bbd1781569bd3724f892ae50adc47d1169a0ea4b43', 13, 1, NULL, '[]', 0, '2023-06-26 07:46:51', '2023-06-26 07:46:51', '2024-06-26 13:16:51');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, 1, 'test', 'a2i1e1ACrmKlG5oUhrSoLGKuUPMOEQyCMAuKrcoD', 'http://127.0.0.1:8000/api/auth/callback', 0, 1, 0, '2023-06-10 01:12:22', '2023-06-10 01:12:22');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('6b3f26a6ab3f3343d5f5957aa087db689bee6d74518c3830ce0d77f3afd5231beab2454475b00e3c', '3cbf63de05ee4c8fca851089880884f8b7192457ed70de36b856eac8bc225704412ab04c1e9ba3a4', 0, '2024-06-10 07:39:06'),
('9bc683ed3d3b6c2ef56b32e97c26c607d8c895cd3da6a0471152cc9287bbfd5020c55cc3f18e34aa', 'b076e06be9068c918cc895d65e07b4e7e836f5be59bd22a9a8046b6813715a76ee262ded728aac9f', 0, '2024-06-10 07:29:54'),
('a9ae51d322bdc0cd18882fe2122ffc389adcf23d95335308aee9fba1f84161779c611996ec8d9285', 'd01b2be3b7ce79fc38894f17f3052f300bbf41bbd1781569bd3724f892ae50adc47d1169a0ea4b43', 0, '2024-06-26 13:16:51'),
('b5df18c579e7dcf7c8d3195ece30ed2867de4417e521f4ce27c5ed693ec4fdcde309c313df2c878b', '5ce05e28ae6f97b22755b31948a521699ad8fd5c38d8fefd596431e9737f055e4bd12c85910dce6e', 0, '2024-06-10 08:55:06'),
('ba0a81a02b0b0c08c26448058a04e1c1375be853176ea19cf7aa12999efe9f87d4fc00ce31084db0', '1febbf9efca1f23bfdc3c09f71f948f0ddbab95af98af9888613e84b985ca010290fb79deb43dc26', 0, '2024-06-26 13:17:00'),
('bbd84f6637e76682ec6259177d57377776250b66077c0d2bf310917cb52335553b130d797ae32588', '48db8fafc31f1d482f389f799eacdc4c065c3bab236eec80099c062e70482c3a0eaab5ec54a3ccfd', 0, '2024-06-26 13:14:09'),
('eb2a03b885ab8588d8b0084b2190429695031883ad7ad9ab4f1ba659012591ec35b5326c0277e900', '34ce7310ea003d101474ba23e430734c5e09b3dab1ff521efc11547e493360e52c1c801f8ee3ff73', 0, '2024-06-10 07:35:29');

-- --------------------------------------------------------

--
-- Table structure for table `organisations`
--

CREATE TABLE `organisations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_user_id` int(11) NOT NULL,
  `trial_end` datetime DEFAULT NULL,
  `subscribed` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `organisations`
--

INSERT INTO `organisations` (`id`, `name`, `owner_user_id`, `trial_end`, `subscribed`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Mumbai', 13, '2023-07-26 14:34:48', 0, '2023-06-26 09:04:48', '2023-06-26 09:04:48', NULL),
(2, 'Vashi Mumbai', 13, '2023-07-26 14:37:22', 0, '2023-06-26 09:07:22', '2023-06-26 09:07:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Arnoldo Hammes', 'mariela95@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-06-10 00:55:08', '2023-06-10 00:55:08'),
(2, 'Annabel Simonis DVM', 'paul.lowe@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-06-10 00:55:08', '2023-06-10 00:55:08'),
(3, 'Marcelo Schimmel Jr.', 'lemuel.rutherford@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-06-10 00:55:08', '2023-06-10 00:55:08'),
(4, 'Prof. Salvatore Kutch', 'sarah72@example.org', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-06-10 00:55:08', '2023-06-10 00:55:08'),
(5, 'Prof. Elwyn Friesen IV', 'hrowe@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-06-10 00:55:08', '2023-06-10 00:55:08'),
(6, 'Miss Idell Kihn', 'armstrong.catalina@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-06-10 00:55:08', '2023-06-10 00:55:08'),
(7, 'Miss Piper Powlowski V', 'jovan.armstrong@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-06-10 00:55:08', '2023-06-10 00:55:08'),
(8, 'Albina Beahan', 'ivah88@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-06-10 00:55:08', '2023-06-10 00:55:08'),
(9, 'Mr. Nicholaus Sipes', 'tromp.krystina@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-06-10 00:55:08', '2023-06-10 00:55:08'),
(10, 'Virginia Crona', 'gerhold.alexane@example.org', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-06-10 00:55:08', '2023-06-10 00:55:08'),
(11, 'Waseem Test Account', 'waseem.momin7@gmail.com', '$2a$12$2SoLrIiOdcng0E70K2Sx9.UXFDp5zXm5EqjUFZ0Yi247sSpDgllXG', '2023-06-10 01:32:39', '2023-06-10 01:32:39'),
(13, 'Test Account', 'test@test.com', '$2y$10$yk6PxVv6aPA3rCOtc..QQen47w.1H6MGpHA5ENsLCdA6f1FsynYV6', '2023-06-10 01:49:00', '2023-06-10 01:49:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisations`
--
ALTER TABLE `organisations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `organisations`
--
ALTER TABLE `organisations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
