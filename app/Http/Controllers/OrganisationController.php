<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Organisation;
use App\Services\OrganisationService;
use Illuminate\Http\JsonResponse;
use App\Serializers\DefaultSerializer;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;


namespace App\Http\Controllers;

use App\Organisation;
use App\Services\OrganisationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Validator;

/**
 * Class OrganisationController
 * @package App\Http\Controllers
 */
class OrganisationController extends ApiController
{
    /**
     * @param OrganisationService $service
     *
     * @return JsonResponse
     */
    public function store(OrganisationService $service): JsonResponse
    {
        $errors = [];
        $status = false;
        $inputData = $this->request->all();
        /** Validate input. */
        $validator = Validator::make($inputData, [
            'name' => 'required|unique:organisations,name'
        ],
        [
            'name.required' => 'Organisation name is required.'
        ], );

        if ($validator->fails()){
            $errors[] = $validator->errors();
        } else {
            try {
                /** @var Organisation $organisation */
                $organisation = $service->createOrganisation($this->request->all());

                return $this
                    ->transformItem('organisation', $organisation, ['user'])
                    ->respond();
            } catch (\Exception $e) {
                $errors[] = [
                    "error" => $e->getMessage(),
                ];
            }
        }

        return response()->json(["status" => $status, "errors" => $errors]);
    }

    /**
     * @param OrganisationService $service
     *
     * @return JsonResponse
     */
    public function listAll(OrganisationService $service): JsonResponse {
        $inputData = $this->request->all();
        $errors = [];        
        $status = false;
        try {
            /** If filter param is not sent set deafult to all */
            $filter = !empty($inputData['filter']) ? $inputData['filter'] : 'all';
            $organisations = $service->listOrganisations($filter);
            return $this
                ->transformCollection('organisations', $organisations)
                ->respond();
        } catch (\Exception $e) {
            //$message = 'Oops! Something went wrong on server.';
            $errors[] = [
                "error" => $e->getMessage(),
            ];
        }
        return response()->json(["status" => $status, "errors" => $errors]);
    }
}